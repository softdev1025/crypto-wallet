const _fetch = {
  fetchJson: function(url, body, method, cors=true){
    const params = {
      method: method,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body)
    };
    if (!cors) {
      Object.assign(params, {mode: 'no-cors'})
    }
    return new Promise(
      function(resolve, reject){
        if(method === 'GET'){
          delete params['body']
        }
        fetch(url, params)
          .then(function(response) {
            return response.json();
        }, (error) => {
          return { error: error }
        }).then(function(json){
          resolve(json);
        }).catch(function(error){
          reject(error);
        });
      }
    )
  },
  fetchText: function(url, body, method, cors=true){
    const params = {
      method: method,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body)
    };
    if (!cors) {
      Object.assign(params, {mode: 'no-cors'})
    }
    return new Promise(
      function(resolve, reject){
        if(method === 'GET'){
          delete params['body']
        }
        fetch(url, params)
          .then(function(response) {
            return response.text();
        }, (error) => {
          return error
        }).then(function(text){
          resolve(text);
        }).catch(function(error){
          reject(error);
        });
      }
    )
  },
}

export default _fetch;