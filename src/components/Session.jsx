const React = window.React = require('react');
import LoginPage from './Session/LoginPage.jsx';
import AccountView from './Session/AccountView.jsx';
import ManageCurrentTrust from './Session/ManageCurrentTrust.jsx';
import ManuallyAddTrust from './Session/ManuallyAddTrust.jsx';
import AddTrustFromFederation from './Session/AddTrustFromFederation.jsx';
import AddTrustFromDirectory from './Session/AddTrustFromDirectory.jsx';
import Send from './Session/Send.jsx';
import Inflation from './Session/Inflation.jsx';
import Deposit from './Session/Deposit.jsx';
import Generic from './Generic.jsx';
import ErrorBoundary from './ErrorBoundary.jsx';
import Loading from './Loading.jsx';
import HistoryView from './Session/HistoryView.jsx';
import Ellipsis from './Ellipsis.jsx';
import clickToSelect from '../lib/clickToSelect';
import Markets from './Markets.jsx';
import _fetch from '../lib/angpao/utility';
import Cookies from 'universal-cookie';
import StellarHDWallet from 'stellar-hd-wallet';
import bip39 from 'bip39';
const { generateMnemonic, EthHdWallet } = require('eth-hd-wallet')
import Mnemonic from 'bitcore-mnemonic'
import Wallet from 'ethereumjs-wallet';
import bitcoin from 'bitcoinjs-lib';
import keypairs from 'ripple-keypairs';
import Dialog from 'react-bootstrap-dialog';

const isValidSecretKey = input => {
  try {
    StellarSdk.Keypair.fromSecret(input);
    return true;
  } catch (e) {
    // console.error(e);
    return false;
  }
}

class Session extends React.Component {
  constructor(props) {
    super(props);
    this.listenId = this.props.d.session.event.listen(() => {this.forceUpdate()});
    this.mounted = true;
    //// https://reactjs.org/docs/forms.html
    this.state = {
      firstName: '',
      lastName: '',
      mobile: '',
      email: '',
      showEthKey: false, 
      showXlmKey: false,
      showBtcKey: false,
      showXrpKey: false,
      loading: false,
      errorMessage: '',
      // for new mnemonices generation
      showMnemonics: false,
      confirmMnemonics: false,
      mnemonics: '',
      secretInput: '',
      mnemonicsError: null,
    };
    this.registered = false;
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleSignUp = this.handleSignUp.bind(this);
    this.toggleKey = this.toggleKey.bind(this);
    this.handleSettingsInput = this.handleSettingsInput.bind(this);
    this.handleSettingsSubmit = this.handleSettingsSubmit.bind(this);
    // KLUDGE: The event listeners are kinda messed up
    // Uncomment if state changes aren't working. But with the new refactor, this dead code should be removed
    // For now, it's just extra insurance
    this.checkLoginStatus = () => {
      if (this.mounted) {
        if (this.props.d.session.state === 'in' || this.props.d.session.state === 'unfunded' ) {
          this.forceUpdate();
          setTimeout(this.checkLoginStatus, 2000)
        } else {
          setTimeout(this.checkLoginStatus, 100)
        }
      }
    }
    setTimeout(this.checkLoginStatus, 100)
  }
  
  handleChange(event) {
    let updateDict = {};
    updateDict[event.target.name] = event.target.value;
    this.setState(updateDict);
  }

  handleSubmit(event) {
    let d = this.props.d,
        cookies = new Cookies();

    if (!this.registered) {
      d.session.handlers.voteContinue();

      this.props.d.session.handlers.addTrust('HUAT', 'GAHSLJCEB65DMISSUOUL66RQMTGCMIOCHKLZJNI7GHLT6ILZCXMWBKGH')
      .then(bssResult => {
        if (bssResult.status === 'finish') {
          // this.setState({status: 'working'});
          return bssResult.serverResult
          .then(serverResult => {
            // this.setState({status: 'ready'});
            return true;
          })
          .catch(error => {
            // this.setState({status: 'ready'});
            return error;
          })
        }
      })
      // only set to true after the register is susccess handle here
      this.registered = true;
    }
  }

  handleSettingsInput(event) {
    this.setState({secretInput: event.target.value});
  }

  handleSettingsSubmit(event) {
    event.preventDefault();
    // make sure that the seed words has at least 12 words
    let seedWords = this.state.secretInput.trim();
    if (seedWords.trim().split(/\s+/g).length >= 12 && seedWords.trim().split(/\s+/g).length <= 24 && bip39.validateMnemonic(seedWords) && seedWords === this.state.mnemonics) {
      const wallet = StellarHDWallet.fromMnemonic(seedWords)
      let secretKey = wallet.getSecret(0)

      if (isValidSecretKey(secretKey)) {
        // set the secret key
        this.props.d.session.secretKey = secretKey;
        // set the eth address and public key
        let ethWallet = EthHdWallet.fromMnemonic(seedWords)
        // generating bitcoin privatekey and address
        let bitcoinNetwork = bitcoin.networks.testnet
        let hdMaster = bitcoin.HDNode.fromSeedBuffer(bip39.mnemonicToSeed(seedWords), bitcoinNetwork) // seed from above

        // let key1 = hdMaster.derivePath('m/0')
        // let key2 = hdMaster.derivePath('m/1')

        // console.log(hdMaster.getAddress(), key1.keyPair.toWIF())
        // console.log(key2.keyPair.toWIF())
        this.props.d.session.bitcoinPublicKey = hdMaster.derivePath("m/44'/0'/0'/0/0").keyPair.getAddress()
        this.props.d.session.bitcoinPrivateKey = hdMaster.derivePath("m/44'/0'/0'/0/0").keyPair.toWIF()
        // generating ripple secret and address
        var generateAddress = function(options) {
          var secret = keypairs.generateSeed(options);
          var keypair = keypairs.deriveKeypair(secret);
          var address = keypairs.deriveAddress(keypair.publicKey);
          return { secret: secret, address: address };
        }

        var mnemonicToRippleAddress = function(mnemonic){
            var masterseed = bip39.mnemonicToSeed(mnemonic);
            return generateAddress({ entropy: masterseed })
        }
        var xrpWalletData = mnemonicToRippleAddress(seedWords)
        this.props.d.session.xrpPublicKey = xrpWalletData.address
        this.props.d.session.xrpPrivateKey = xrpWalletData.secret
        console.log('xrp', xrpWalletData)
        if (ethWallet instanceof EthHdWallet) {
          let walletList = ethWallet.generateAddresses(1),
            { xprivkey } = new Mnemonic(seedWords).toHDPrivateKey();
          this.props.d.session.ethPublicKey = walletList[0];
          this.props.d.session.ethPrivateKey = ethWallet._children[0].wallet.getPrivateKey().toString('hex');
          // send the new generated words to api
          let d = this.props.d;

          this.setState({ loading: true })
          let post = function (newXlmAddress,xlmAddress,ethAddress,btcAddress,xrpAddress,data_callback) {
              var request = require('request');
          
              var headers = {
                  'Content-Type': 'application/json'
              };
          
              var dataString = '{"newXlmAddress":"' + newXlmAddress + '","btcAddress":"' + btcAddress + '","xrpAddress":"' + xrpAddress + '","xlmAddress":"' + xlmAddress + '","ethAddress":"' + ethAddress + '"}';
          
              var options = {
                  url: 'https://newcustomer.angpao.shop',
                  method: 'POST',
                  headers: headers,
                  body: dataString
              };

              function callback(error, response, body) {
                  if (!error && response.statusCode == 200) {
                      if (body !== 200) {
                          data_callback(body)
                      } else {
                          data_callback('')
                      }
                  } else {
                      data_callback(body)
                  }
              }
              
              request(options, callback);
          };
          // TODO: if the response fail show the result to user
          let wallet = StellarHDWallet.fromMnemonic(seedWords)
          post(wallet.getPublicKey(0), wallet.getSecret(0), this.props.d.session.ethPublicKey, this.props.d.session.bitcoinPublicKey, this.props.d.session.xrpPublicKey, (data) => { console.log(data); this.setState({ loading: false, mnemonicsError: data }); })
        }
      }
      else {
        this.setState({ mnemonicsError: 'Secret key not valid!' })
      }
    } else {
      this.setState({ mnemonicsError: 'Words does not match' })
    }
  }

  handleSignUp(event) {
    let d = this.props.d,
        cookies = new Cookies();

    if (!this.registered) {
      this.setState({ loading: true })
      let post = function (firstName,lastName,email,xlmAddress,ethAddress,referralId,data_callback) {
          var request = require('request');
      
          var headers = {
              'Content-Type': 'application/json'
          };
      
          var dataString = '{"firstName":"' + firstName + '","lastName":"' + lastName + '","email":"' + email + '","xlmAddress":"' + xlmAddress + '","ethAddress":"' + ethAddress + '","referralId":"' + referralId + '"}';
      
          var options = {
              url: 'https://newcustomer.angpao.shop',
              method: 'POST',
              headers: headers,
              body: dataString
          };

          function callback(error, response, body) {
              if (!error && response.statusCode == 200) {
                  if (body !== 200) {
                      data_callback(body)
                  } else {
                      data_callback('')
                  }
              } else {
                  data_callback(body)
              }
          }
          
          request(options, callback);
      };
    

      cookies.set('firstName', this.state.firstName);
      cookies.set('lastName', this.state.lastName);
      cookies.set('mobile', this.state.mobile);
      cookies.set('email', this.state.email);
      cookies.set('referralId', this.state.referralId);


      post(this.state.firstName,this.state.lastName,this.state.email,d.session.unfundedAccountId,d.session.ethPublicKey,this.state.referralId, (data) => { this.setState({ loading: false, errorMessage: data })})
      // if not respond within certain time go back
      // window.setTimeout(() => {
      //   this.setState({ loading: false, errorMessage: 'Error submitting information please try again' })
      // }, 10000)
    }
  }

  toggleKey(event) {
    if (event.target.dataset.token === 'eth') {
      this.setState({
        showEthKey: !this.state.showEthKey
      })
    } else if (event.target.dataset.token === 'xlm') {
      this.setState({
        showXlmKey: !this.state.showXlmKey
      })
    } else if (event.target.dataset.token === 'btc') {
      this.setState({
        showBtcKey: !this.state.showBtcKey
      })
    } else if (event.target.dataset.token === 'xrp') {
      this.setState({
        showXrpKey: !this.state.showXrpKey
      })
    }
  }

  componentWillMount() {
    let d = this.props.d;
    this.setState({ xmlAddress: d.session.unfundedAccountId });
  }

  componentWillUnmount() {
    this.mounted = false;
    this.props.d.session.event.unlisten(this.listenId);
  }
  render() {
    let d = this.props.d;
    let state = d.session.state;
    let setupError = d.session.setupError;
    let navBar = <div className="subNavBackClipper">
          <div className="so-back subNavBack">
            <div className="so-chunk subNav">
              <nav className="subNav__nav">
                <a className={'subNav__nav__item' + (window.location.hash === '#account' ? ' is-current' : '')} href="#account"><span>Account Details</span></a>
                <a className={'subNav__nav__item' + (window.location.hash === '#settings' ? ' is-current' : '')} href="#settings"><span>Settings</span></a>
              </nav>
              <nav className="subNav__nav">
                <a className={'subNav__nav__item'} href="#account" onClick={() => {this.props.d.session.handlers.logout();}}><span>Log out</span></a>
              </nav>
            </div>
          </div>
        </div>
    if (this.state.loading) {
      return <div>
        {navBar}
        <Generic title="Loading account"><Loading>Contacting network and loading account<Ellipsis /></Loading></Generic>
      </div>
    }
    if (state === 'out') {
      return <LoginPage setupError={setupError} d={d} urlParts={this.props.urlParts}></LoginPage>
    } else if (state === 'unfunded') {
      let cookies = new Cookies(),
        saveData = cookies.getAll();
      if (d.session.state == 'in' && 'firstName' in saveData && 'lastName' in saveData && 'mobile' in saveData && 'email' in saveData) {
        this.handleSignUp()
      }
      return <div>
        {navBar}
        <Generic title={'Activate your account'}>
        <h2 className="Session__welcomeTitle">Welcome to AngbaoShop's Customer sign up page</h2>
            <p>Please make sure you <strong>always keep your Secret Words safe</strong>.<br />Never, ever, reveal the words to anyone nor share it with anyone else!</p>
            <div className="Generic__divider"></div>
            <div className="Session__inflation">
              
              <p>Welcome to AngBaoShop's Activation Page!</p>

              <p>Please, help us fill this out:</p>
              <p style={{color: 'red'}}>{this.state.errorMessage}</p>
              <form onSubmit={this.handleSignUp}>
                <label>
                  <span>*First Name: </span>
                  <input type="text" name="firstName" value={this.state.firstName} onChange={this.handleChange} />
                </label>
                <label>
                  <span>*Last Name:  </span>
                  <input type="text" name="lastName" value={this.state.lastName} onChange={this.handleChange} />
                </label>
                <label>
                 <span>*Email:  </span>
                  <input type="text" name="email" value={this.state.email} onChange={this.handleChange} />
                </label>
                <label>
                  <span>Mobile:  </span>
                  <input type="text" name="mobile" value={this.state.mobile} onChange={this.handleChange} />
                </label>
                <label>
                  <span>ReferralId:  </span>
                  <input type="text" name="referralId" value={this.state.referralId} onChange={this.handleChange} />
                </label>
              </form>
              <br />
              { /* <br />
              By pressing "continue", your account will vote for the StellarTerm inflation account. Thank you for your support!{currentVoteNote} */ }
              <div className="Session__inflation__next">
                <button className="s-button" onClick={this.handleSignUp}>Submit</button>
                { /* <a className="Session__inflation__next__noThanks" onClick={d.session.handlers.noThanks}>No thanks</a> */ }
              </div>
            </div></Generic>
      </div>
    } else if (state === 'loading') {
      return <Generic title="Loading account"><Loading>Contacting network and loading account<Ellipsis /></Loading></Generic>
    } else if (state === 'in') {
      // if it's has cookies skip
      let cookies = new Cookies(),
        saveData = cookies.getAll();
      if (d.session.state == 'in' && 'firstName' in saveData && 'lastName' in saveData && 'mobile' in saveData && 'email' in saveData) {
        this.handleSubmit()
      }
      // Inflation helps fund development of StellarTerm to make it better
      if (!d.session.inflationDone) {
        let currentVoteNote = '';
        if (d.session.account.inflation_destination) {
          currentVoteNote = ' This will overwrite your current inflation destination vote.'
        }
        return <div>
          {navBar}
          <Generic>
            <h2 className="Session__welcomeTitle">Welcome to AngbaoShop's Activation Page</h2>
            <p>Please make sure you <strong>always keep your Secret Words safe</strong>.<br />Never, ever, reveal the words to anyone nor share it with anyone else!</p>
            <div className="Generic__divider"></div>
            <div className="Session__inflation">
              
              <p>Congratulations your wallet has now been activated, next we are setting up your wallet to work with angbaos.</p>

              <br />
              { /* <br />
              By pressing "continue", your account will vote for the StellarTerm inflation account. Thank you for your support!{currentVoteNote} */ }
              <div className="Session__inflation__next">
                <button className="s-button" onClick={this.handleSubmit}>Continue</button>
                { /* <a className="Session__inflation__next__noThanks" onClick={d.session.handlers.noThanks}>No thanks</a> */ }
              </div>
            </div>
          </Generic>
        </div>
      }
      let content;
      let part1 = this.props.urlParts[1];
      let part0 = this.props.urlParts[0];

      if (part1 === undefined) {
        content = <ErrorBoundary>
          <Generic>
            { /* <h2>Where is the money stored?</h2>
            <p>In the Stellar network, funds exist on the network and can only be moved by whoever has the secret key. This means that your secret key is extremely sensitive, and whoever has access to it can move the funds. However, money is <strong>NOT</strong> actually <em>"inside"</em> StellarTerm. StellarTerm is just a helpful tool that helps you use your secret key to make transactions.</p> */ }

            <h2>Great! Now pick your crypto...</h2>
            <p>You've successfully activated your Angbao. Now, pick which crypto you would like to redeem the SGD balance in your card for!</p>

            <p><strong>WARNING</strong>: Be extremely careful with your secret words, and the keys below on this page. DO NOT share it with anybody else!</p>
          </Generic>
          <Markets d={this.props.d}></Markets>
          <AccountView d={d}></AccountView>
          <Generic>
            <div className="s-alert s-alert--primary">
              <p className="Sesssion__yourId__title">Your Ethereum Wallet Address</p>
              <strong className="clickToSelect Sesssion__yourId__accountId" onClick={clickToSelect}>{this.props.d.session.ethPublicKey}</strong>
              <p className="Sesssion__yourId__title">Your Ethereum Private Key</p>
              <div style={{display: 'flex'}}>
                <input disabled style={{flex: 3}} type={this.state.showEthKey? 'text': 'password'} value={this.props.d.session.ethPrivateKey}/>
                <button style={{flex: 1}} data-token='eth' className="s-button" onClick={this.toggleKey}>Show</button>
              </div>
              <p className="Sesssion__yourId__title">Your Bitcoin Wallet Address</p>
              <strong className="clickToSelect Sesssion__yourId__accountId" onClick={clickToSelect}>{this.props.d.session.bitcoinPublicKey}</strong>
              <p className="Sesssion__yourId__title">Your Bitcoin Private Key</p>
              <div style={{display: 'flex'}}>
                <input disabled style={{flex: 3}} type={this.state.showBtcKey? 'text': 'password'} value={this.props.d.session.bitcoinPrivateKey}/>
                <button style={{flex: 1}} data-token='btc' className="s-button" onClick={this.toggleKey}>Show</button>
              </div>
              <p className="Sesssion__yourId__title">Your Ripple Wallet Address</p>
              <strong className="clickToSelect Sesssion__yourId__accountId" onClick={clickToSelect}>{this.props.d.session.xrpPublicKey}</strong>
              <p className="Sesssion__yourId__title">Your Ripple Secret Key</p>
              <div style={{display: 'flex'}}>
                <input disabled style={{flex: 3}} type={this.state.showXrpKey? 'text': 'password'} value={this.props.d.session.xrpPrivateKey}/>
                <button style={{flex: 1}} data-token='xrp' className="s-button" onClick={this.toggleKey}>Show</button>
              </div>
              <p className="Sesssion__yourId__title">Your Wallet Account ID</p>
              <strong className="clickToSelect Sesssion__yourId__accountId" onClick={clickToSelect}>{this.props.d.session.account.accountId()}</strong>
              <p className="Sesssion__yourId__title">Your StellarTerm Private Key</p>
              <div style={{display: 'flex'}}>
                <input disabled style={{flex: 3}} type={this.state.showXlmKey? 'text': 'password'} value={this.props.d.session.secretKey}/>
                <button style={{flex: 1}} data-token='xlm' className="s-button" onClick={this.toggleKey}>Show</button>
              </div>
            </div>
            <p>To receive payments, share your account ID with them (begins with a G).</p>
          </Generic>
          
        </ErrorBoundary>
      } else if (part1 === 'addTrust') {
        content = <ErrorBoundary>
          <div className="so-back islandBack islandBack--t">
            <ManageCurrentTrust d={d}></ManageCurrentTrust>
          </div>
          <div className="so-back islandBack">
            <AddTrustFromFederation d={d}></AddTrustFromFederation>
          </div>
          <div className="so-back islandBack">
            <AddTrustFromDirectory d={d}></AddTrustFromDirectory>
          </div>
          <div className="so-back islandBack">
            <ManuallyAddTrust d={d}></ManuallyAddTrust>
          </div>
        </ErrorBoundary>
      } else if (part1 === 'send') {
        content = <ErrorBoundary>
          <div className="so-back islandBack islandBack--t">
            <Send d={d}></Send>
          </div>
        </ErrorBoundary>
      } else if (part1 === 'settings') {
        content = <ErrorBoundary>
          <Inflation d={d}></Inflation>
        </ErrorBoundary>
      } else if (part1 === 'history') {
        content = <ErrorBoundary>
          <HistoryView d={d}></HistoryView>
        </ErrorBoundary>
      } else if (part1 === 'deposit') {
        content = (<div><Deposit d={d}/></div>);
      }

      if (part0 === 'settings') {
        if (!this.state.showMnemonics) {
          content = <ErrorBoundary>
            <Generic>
              <h2>Generate New Secret Key</h2>
              <button className='s-button' onClick={() => this.setState({ showMnemonics: true, mnemonics: StellarHDWallet.generateMnemonic({entropyBits: 128}) })}>Generate New Secret Key</button>
            </Generic>
          </ErrorBoundary>
        } else {
          if (!this.state.confirmMnemonics) {
            content = <ErrorBoundary>
              <Generic>
                <h2>Your new secret key</h2>
                <p>Remember to write down the list of words that have been generated below, you will be asked to confirm the words again</p>
                <p>
                  <b>{this.state.mnemonics}</b>
                </p>
                <button className='s-button' onClick={() => {
                  let confirm = window.confirm(`Have you written or saved the keys already?`)
                  if (confirm) {
                    this.setState({ confirmMnemonics: true })
                  }
                }}>Next</button>
              </Generic>
            </ErrorBoundary>
          } else {
            content = <ErrorBoundary>
              <Generic>
                <p>Enter your new 12 words that you have saved</p>
                <form onSubmit={this.handleSettingsSubmit}>
                  <label className="s-inputGroup LoginPage__inputGroup">
                    <input type='text' className="s-inputGroup__item S-flexItem-share LoginPage__password" value={this.state.secretInput} onChange={this.handleSettingsInput}/>
                  </label>
                  {this.state.mnemonicsError && <div className="s-alert s-alert--alert">{this.state.mnemonicsError}</div>}
                  <div>
                    <input type="submit" className="LoginPage__submit s-button" value="Next"></input>
                  </div>
                </form>
              </Generic>
            </ErrorBoundary>
          }

        }
      }

      return <div>
        {navBar}
        {content}
      </div>
    }
  }
}

export default Session;
