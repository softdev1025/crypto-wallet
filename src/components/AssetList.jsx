const React = window.React = require('react');
import AssetCard2 from './AssetCard2.jsx';
import Loading from './Loading.jsx';
import Ellipsis from './Ellipsis.jsx';
import Printify from '../lib/Printify';
import directory from '../directory.js';
import Stellarify from '../lib/Stellarify';
import Format from '../lib/Format';
import Driver from '../lib/Driver';
import Validate from '../lib/Validate';
import MagicSpoon from '../lib/MagicSpoon';
import _ from 'lodash';
import getPriceTickerSgd from '../lib/angpao/priceTickerSgd';
import CoinMarketCap from 'node-coinmarketcap';

export default class AssetList extends React.Component {
  constructor(props) {
    super(props);
    this.dTicker = props.d.ticker;
    this.coinmarketcap = new CoinMarketCap({
        events: true,
        refresh: 60,
        convert: "SGD"
    });
    this.priceSgd = {};
    this.listenId = this.dTicker.event.listen(() => {this.forceUpdate()});
    this.handleBuy = this.handleBuy.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this, _, directory, Validate, MagicSpoon, Stellarify);
    this.updateAvailableLists = this.updateAvailableLists.bind(this, _, directory, Validate, MagicSpoon, Stellarify);
  }
  async handleSubmit(_, directory, Validate, MagicSpoon, Stellarify) {
    let d = this.props.d;
    let tx = await MagicSpoon.buildTxSendPayment(d.Server, d.session.account, {
      destination: d.send.accountId,
      asset: d.send.step2.availability.asset,
      amount: d.send.step3.amount,
      memo: {
        type: d.send.memoType,
        content: d.send.memoContent,
      },
    });
    console.log('Tx', tx)
    let bssResult = await d.session.handlers.buildSignSubmit(tx);
    console.log('result', bssResult)
    if (bssResult.status === 'finish') {
      // this.state = 'pending';
      console.log('pending')
      let result = await bssResult.serverResult;
      // this.txId = result.hash;
      window.alert(`Success! Your transaction hash: ${result.hash}`);
      // this.state = 'success';
    }
  }
  updateAvailableLists(_, directory, Validate, MagicSpoon, Stellarify) {
    let d = this.props.d;
    // update the available lists
    // Calculate the assets that you can send to the destination
    d.send.availableAssets = {};
    d.send.availableAssets[Stellarify.assetToSlug(new StellarSdk.Asset.native())] = {
      asset: new StellarSdk.Asset.native(),
      sendable: true,
    };
    const senderTrusts = {};
    const receiverTrusts = {};

    let sendableAssets = {};
    let unSendableAssets = {};

    _.each(d.session.account.balances, (balance) => {
      const asset = Stellarify.asset(balance);
      const slug = Stellarify.assetToSlug(asset);
      if (asset.isNative()) {
        return;
      }
      if (asset.issuer === d.send.targetAccount.accountId()) {
        // Edgecase: Receiver is the issuer of the asset
        // Note: Accounts cant extend trust to themselves, so no further edgecases on this situation
        d.send.availableAssets[slug] = {
          asset: asset,
          sendable: true
        };
        receiverTrusts[slug] = true;
      } else {
        senderTrusts[slug] = true;
      }
    });

    _.each(d.send.targetAccount.balances, (balance) => {
      const asset = Stellarify.asset(balance);
      const slug = Stellarify.assetToSlug(asset);
      if (asset.isNative()) {
        return;
      }
      // We don't really care about the usecase of sending to issuer.
      receiverTrusts[slug] = true;
    });

    _.each(d.send.targetAccount.balances, (balance) => {
      const asset = Stellarify.asset(balance);
      const slug = Stellarify.assetToSlug(asset);
      if (senderTrusts.hasOwnProperty(slug)) {
        sendableAssets[slug] = {
          asset: asset,
          sendable: true,
        };
      } else if (asset.getIssuer() === d.session.account.accountId()) {
        // Edgecase: Sender is the issuer of the asset
        sendableAssets[slug] = {
          asset: asset,
          sendable: true,
        };
      } else {
        // Asset can't be sent.
      }
    });

    // Show stuff the recipient doesn't trust
    _.each(d.session.account.balances, balance => {
      const asset = Stellarify.asset(balance);
      const slug = Stellarify.assetToSlug(asset);
      if (asset.isNative()) {
        return;
      }

      if (!sendableAssets.hasOwnProperty(slug) && !receiverTrusts.hasOwnProperty(slug)) {
        unSendableAssets[slug] = {
          asset: asset,
          sendable: false,
          reason: 'receiverNoTrust',
        };
      }
    })

    _.each(sendableAssets, (availability, slug) => {
      d.send.availableAssets[slug] = availability;
    });
    _.each(unSendableAssets, (availability, slug) => {
      d.send.availableAssets[slug] = availability;
    });

    if (directory.destinations.hasOwnProperty(this.accountId)) {
      let whitelist = directory.destinations[this.accountId].acceptedAssetsWhitelist;
      if (whitelist) {
        this.availableAssets = _.map(this.availableAssets, (availability, slug) => {
          if (whitelist.indexOf(slug) === -1) {
            availability.sendable = false;
            availability.reason = 'assetNotWhitelisted';
          }
          return availability
        });
      }
    }
  }
  handleBuy(event, _, directory, Validate, MagicSpoon, Stellarify) {
    let d = this.props.d,
        code = event.target.dataset.code,
        slug = event.target.dataset.slug,
        state = d.send.state;
    // only buy if it's login if not redirect to account
    if (!d.session.secretKey) {
      return window.location.hash = '#account';
    }
    try {
      // list of hard coded items
      d.send.accountId = 'GAHSLJCEB65DMISSUOUL66RQMTGCMIOCHKLZJNI7GHLT6ILZCXMWBKGH';
      d.send.step1.destInput = 'GAHSLJCEB65DMISSUOUL66RQMTGCMIOCHKLZJNI7GHLT6ILZCXMWBKGH';
      d.send.memoType = 'MEMO_TEXT';
      d.send.memoContent = `BUY:${code}`;
      // d.send.handlers.step1Next();
      // amount
      d.send.step3.amount = String(d.session.account.getBalance(new StellarSdk.Asset('HUAT', 'GAHSLJCEB65DMISSUOUL66RQMTGCMIOCHKLZJNI7GHLT6ILZCXMWBKGH')));
      // d.send.step3.amount = '0.001'
      let confirm = window.confirm(`Do you want to trade your ${d.send.step3.amount} HUAT for ${code}?`)
      if (confirm) {
        if (Validate.publicKey(d.send.step1.destInput).ready) {
          console.log('Before', d.send)
          d.send.accountId = d.send.step1.destInput;

          // Check for memo requirements in the destination
          if (directory.destinations.hasOwnProperty(d.send.accountId)) {
            const destination = directory.destinations[d.send.accountId];
            if (destination.requiredMemoType) {
              d.send.memoRequired = true;
              d.send.memoType = destination.requiredMemoType;
            }
          }

          // Async loading of target account
          if (Validate.publicKey(d.send.accountId).ready) {
            d.Server.loadAccount(d.send.accountId)
            .then((account) => {
              console.log('checking account', account)
              if (account.id === d.send.accountId) {
                // Prevent race conditions using this check
                d.send.targetAccount = account;
                this.updateAvailableLists();
                // select asset
                d.send.step2.availability = d.send.availableAssets['HUAT-GAHSLJCEB65DMISSUOUL66RQMTGCMIOCHKLZJNI7GHLT6ILZCXMWBKGH'];
                console.log('After 1', d.send)
                this.handleSubmit();
              }
            })
            .catch(() => {});
          }
        } else if (Validate.address(d.send.step1.destInput).ready) {
          // Prevent race race conditions
          const destInput = d.send.step1.destInput;

          StellarSdk.FederationServer.resolve(d.send.step1.destInput)
          .then((federationRecord) => {
            if (destInput !== d.send.step1.destInput) {
              return;
            }
            d.send.address = d.send.step1.destInput;
            if (!Validate.publicKey(federationRecord.account_id).ready) {
              throw new Error('Invalid account_id from federation response');
            }
            d.send.accountId = federationRecord.account_id;

            if (federationRecord.memo_type) {
              switch (federationRecord.memo_type) {
                case 'id':
                  d.send.memoType = 'MEMO_ID';
                  break;
                case 'text':
                  d.send.memoType = 'MEMO_TEXT';
                  break;
                case 'hash':
                  d.send.memoType = 'MEMO_HASH';
                  break;
                case 'return':
                  d.send.memoType = 'MEMO_RETURN';
                  break;
                default:
                  throw new Error('Invalid memo_type from federation response');
              }

              d.send.memoRequired = true;
            }

            if (federationRecord.memo) {
              d.send.memoContent = federationRecord.memo;
              d.send.memoContentLocked = true;
            }

            if (Validate.publicKey(d.send.accountId).ready) {
              d.Server.loadAccount(d.send.accountId)
              .then((account) => {
                if (account.id === d.send.accountId) {
                  // Prevent race conditions using this check
                  d.send.targetAccount = account;
                  this.updateAvailableLists();
                  d.send.step2.availability = d.send.availableAssets['HUAT-GAHSLJCEB65DMISSUOUL66RQMTGCMIOCHKLZJNI7GHLT6ILZCXMWBKGH'];
                  console.log('After 2', d.send)
                  this.handleSubmit();
                }
              })
              .catch(() => {});
            }
          })
          .catch(err => {
            if (destInput !== d.send.step1.destInput) {
              return;
            }
            d.send.addressNotFound = true;
            d.send.event.trigger();
          });
        }
      }
    } catch (err) {
      console.log({'error': err.message, 'errorDetails': JSON.stringify(err, null, 2)});
      throw err;
    }
  }
  componentWillMount() {
    this.dTicker.data.assets = [
      {
        "id": "BTC",
        "code": "BTC",
        "slug": "BTC",
        "domain": "bitcoin",
        "issuer": "https://www.bitcoin.org/"
      },
      {
        "id": "ETH",
        "code": "ETH",
        "slug": "ETH",
        "domain": "ethereum",
        "issuer": "https://www.ethereum.org/"
      },
      {
        "id": "XRP",
        "code": "XRP",
        "slug": "XRP",
        "domain": "ripple",
        "issuer": "https://www.ripple.com/"
      },
      {
      "id": "XLM",
      "code": "XLM",
      "slug": "XLM",
      "domain": "stellar",
      "issuer": "https://www.stellar.org/"
    },
    {
      "id": "TOP3",
      "code": "TOP3",
      "slug": "TOP3",
      "domain": "angbaoshop",
      "issuer": "https://index.angbaoshop.com/"
    },
    {
      "id": "OP-XRP-SGD",
      "code": "OP-XRP-SGD",
      "slug": "OP-XRP-SGD",
      "domain": "angbaoshop",
      "issuer": "https://index.angbaoshop.com/"
    }
];
    _.each(this.dTicker.data.assets, (asset, index) => {
      this.coinmarketcap.on(asset.id, (coin) => {
        this.priceSgd[asset.id] = `$${coin.price_sgd}`;
      });
    })
  }
  componentWillUnmount() {
    this.dTicker.event.unlisten(this.listenId);
  }
  render() {
    if (!this.dTicker.ready) {
      return <Loading size="large">Loading Stellar market data<Ellipsis /></Loading>
    }

    let rows = [];
    // filter the ticker here for the allowed assets
    this.dTicker.data.assets = [
      {
        "id": "BTC",
        "code": "BTC",
        "slug": "BTC",
        "domain": "bitcoin",
        "issuer": "https://www.bitcoin.org/"
      },
      {
        "id": "ETH",
        "code": "ETH",
        "slug": "ETH",
        "domain": "ethereum",
        "issuer": "https://www.ethereum.org/"
      },
      {
        "id": "XRP",
        "code": "XRP",
        "slug": "XRP",
        "domain": "ripple",
        "issuer": "https://www.ripple.com/"
      },
      {
      "id": "XLM",
      "code": "XLM",
      "slug": "XLM",
      "domain": "stellar",
      "issuer": "https://www.stellar.org/"
    },
    {
      "id": "TOP3",
      "code": "TOP3",
      "slug": "TOP3",
      "domain": "angbaoshop",
      "issuer": "https://index.angbaoshop.com/"
    },
    {
      "id": "OP-XRP-SGD",
      "code": "OP-XRP-SGD",
      "slug": "OP-XRP-SGD",
      "domain": "angbaoshop",
      "issuer": "https://index.angbaoshop.com/"
    }
];
    _.each(this.dTicker.data.assets, (asset, index) => {
      if (this.props.limit && index >= this.props.limit) {
        return;
      }
      let priceSgd = 0;
      let assetPriceSgd = <span>{this.priceSgd[asset.id] || 'getting price ...'}</span>
      let tradeLink;
      tradeLink = <button onClick={(event) => this.handleBuy(event, _, directory, Validate, MagicSpoon, Stellarify)} data-code={asset.code} data-slug={asset.slug} className="s-button AssetList__asset__amount__trade">Redeem</button>

      rows.push(<div key={'asset-' + asset.id} className="AssetList__asset">
        <div className="AssetList__asset__assetCard"><AssetCard2 code={asset.code} issuer={asset.issuer} boxy={false}></AssetCard2></div>
        <div className="AssetList__asset__amount">{assetPriceSgd || '-'}</div>
        <div className="AssetList__asset__amount">{tradeLink}</div>
      </div>);
    })
    return (
      <div className="AssetList">
        <div className="AssetList__head__row">
          <div className="AssetList__head__cell AssetList__head__asset">Asset</div>
          <div className="AssetList__head__cell AssetList__head__amount">Price (SGD)</div>
        </div>
        {rows}
      </div>
    );
  }
};
