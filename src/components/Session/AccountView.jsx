const React = window.React = require('react');
import Stellarify from '../../lib/Stellarify';
import Printify from '../../lib/Printify';
import BalancesTable from './BalancesTable.jsx';
import MinBalance from './MinBalance.jsx';
import _ from 'lodash';
import getEtherBalance from '../../lib/angpao/getEthBalance';
import getBtcBalance from '../../lib/angpao/getBtcBalance';
import getXrpBalance from '../../lib/angpao/getXrpBalance';

export default class AccountView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      wei: 0,
      eth: 0,
      btc: 0,
      xrp: 0,
      xrpError: false
    }
  }

  componentWillMount() {
    let ethBalanceData = getEtherBalance(this.props.d.session.ethPublicKey, (wei, eth) => {
      this.setState({
        wei: wei,
        eth: eth
      })
    }),
    btcBalanceData = getBtcBalance(this.props.d.session.bitcoinPublicKey, (data) => {
      this.setState({
        btc: data
      })
    })
    getXrpBalance(this.props.d.session.xrpPublicKey, (data, error=false) => {
      if (error) {
        this.setState({
          xrp: data.indexOf('actNotFound' >= 0) ? 'Please activate your account using secrect key in order to view the balance' : data,
          xrpError: true
        })
      } else {
        this.setState({
          xrp: data
        })
      }
    })
  }

  render() {
    let huatBalance = String(Math.round(this.props.d.session.account.getBalance(new StellarSdk.Asset('HUAT', 'GAHSLJCEB65DMISSUOUL66RQMTGCMIOCHKLZJNI7GHLT6ILZCXMWBKGH'))),2)
    let xlmBalance = String(this.props.d.session.account.getBalance(new StellarSdk.Asset('XLM', null)))
        // wei = ethBalanceData? ethBalanceData['wei']: 0,
        // eth = ethBalanceData? ethBalanceData['eth']: 0;
    return <div>
      <div className="so-back islandBack">
        <div className="island">
          <div className="island__header">
            <a>Balances</a>
            <a className="Header__nav__item Header__nav__item--link" href="http://localhost:3000/vendorsignup">Vendor Sign Up</a>
          </div>
          <div className="Session__AccountView__content">
            {`You have current balances of ${huatBalance} SGD`}
          </div>
          {xlmBalance > 5 && <div className="Session__AccountView__content">
            {`You have current balances of ${xlmBalance} XLM`}
          </div>}
          <div className="Session__AccountView__content">
            {`You have current balances of ${this.state.eth} ethereum`}
          </div>
          <div className="Session__AccountView__content">
            {`You have current balances of ${this.state.btc} bitcoin`}
          </div>
          <div className="Session__AccountView__content">
            {this.state.xrpError? `${this.state.xrp}` : `You have current balances of ${this.state.xrp} xrps`}
          </div>
        </div>
      </div>
    </div>
  }
}
