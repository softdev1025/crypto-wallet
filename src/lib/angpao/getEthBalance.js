var Web3 = require('web3');



var web3 = new Web3(
	new Web3.providers.HttpProvider('https://rinkeby.infura.io/')
);


// var web3 = new Web3(
// 	new Web3.providers.HttpProvider('https://mainnet.infura.io/')
// );


const getEtherBalance = (address, callback) => {
	web3.eth.getBalance(address, function(error,result){
	    if (!error) {
			let ether = Number(result) / 1000000000000000000
	    	return callback(result,ether)
	        // return { wei: result, eth: web3.utils.fromWei(result) }
	    }
	    else {
	    	console.error(error);
	    	return false;
	    }
	})
}

export default getEtherBalance;