import _fetch from './utility';

const getBtcBalance = (address, callback) => {
	let btcBalance = _fetch.fetchText(`https://testnet.blockexplorer.com/api/addr/${address}/balance`, {}, 'GET')
	btcBalance.then((data) => {
		let btc = data / 100000000
		callback(btc)
	})
}

export default getBtcBalance;