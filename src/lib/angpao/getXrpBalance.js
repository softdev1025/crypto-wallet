// import _fetch from './utility';

// const getXrpBalance = (address, callback) => {
// 	let btcBalance = _fetch.fetchJson(`https://data.ripple.com/v2/accounts/${address}/balances?currency=SGD`, {}, 'GET', false)
// 	btcBalance.then((data) => {
// 		callback(data)
// 	})
// }

// export default getXrpBalance;

const RippleAPI = require('ripple-lib').RippleAPI

const getXrpBalance = (address, callback) => {
	const api = new RippleAPI({server: 'wss://s1.ripple.com:443'});
	api.connect().then(() => {
		api.getBalances(address).then(balances => {
			console.log(JSON.stringify(balances, null, 2));
			callback(balances)
	    	process.exit();
	  }).catch((errorCode, errorMessage, data) => {
	  	callback(String(errorCode), true)
	  });
	});
	api.on('error', (errorCode, errorMessage, data) => {
	  console.log(errorCode + ': ' + errorMessage);
	});	
}

export default getXrpBalance;
