import _fetch from './utility';

const getPriceTickerSgd = (tickerName, tickerCurrency) => {
  // The name/ID for the new entity
  let name = tickerName,
      currency = tickerCurrency? tickerCurrency: 'SGD';
  return _fetch.fetchJson(`https://min-api.cryptocompare.com/data/price?fsym=${name}&tsyms=${currency}`, {}, 'GET');
}

export default getPriceTickerSgd;
